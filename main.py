


import BAC0


bacnet = BAC0.connect(ip='137.226.249.047/24:47808')
#bacnet = BAC0.lite(ip='137.226.249.047')
bacnet.discover()
my_device = BAC0.device('137.226.249.047/24:47808', 0, bacnet)
print(my_device.points)

devices = bacnet.whois(global_broadcast=True)
print('#bacnet.whois#')
print(devices)
print(bacnet.whois(global_broadcast=True)[0])

mac, device_id = bacnet.whois(global_broadcast=True)[0]

print('#mac#')
print(mac)

print('#device_id#')
print(device_id)

print('#bacnet.read#')
print(bacnet.read(f'{mac} device {device_id} objectList'))

